# TFSTATE_VERIFIER

Verification of deployed resource using Terraform State file

## Overview

As a best practice of Terraform development, it is highly recommended to test your deployment. This article [Terraform Code Quality](https://cloudskiff.com/terraform-code-quality/) offers a good overview of testing the resource using well-known libraries and utilities. There are other widely available articles too, which attest to the suggested approach.

One common behavior in most of these solutions involves connecting and querying Azure resources and ensuring if they are in conformance with the desired state.

Also as part of the unit testing strategy, the steps involved:
 - Creating resources from scratch
 - Running various tests and ensuring the deployment is successful.
 - Teardown of the infrastructure.

This is typically done in a secluded environment so that it would not affect development, QA environments accidentally.

In production environments, performing such introspection of the resources would be hard. As in large enterprises, production environments are maintained by a separate ops team and the resources would be in a separate subscription & accessible from only a specific set of networks too.

In production environments, we typically deploy premium solutions and enable more security features too. How do we validate if these were enabled or not?

In this article, I am trying to approach the verification of resources, that have been deployed by Terraform, based on its state file. 

## Opinionated approach to verification
We start off with the base understanding 
 - Terraforms after deploying the resources maintain the states. If there were any errors in deploying a particular resource, it would be errored out and not maintained in the state file.
 - Resources deployed using Terraform are not altered manually under normal conditions. 

Although by reading the state file, we can confirm that a specific resource got deployed. But we want to answer queries beyond that, Forex:
 - we want to ensure "blob public access" is disabled.
 - Is the storage account of type RAGRS.
 - Is the object replication on the storage account configured?
 - Does a specific ADF instance have the System Managed Identity defined?

Some or most of these features might be turned off in DEV, QA environment but not necessarily in the production environment.

**NOTE:** This article is centered around Azure resources deployed using Terraform, but I do believe the pattern demonstrated here would apply to other implementation performed using Terraform.

### Adoption methodology

The Terraform state file is a simple JSON file. As my knowledge goes, in system development Go, Ruby are widely adopted languages. However, learning these languages to parse a JSON file has not been a compelling reason. 

I and most of my team members are experienced more in application and data analytics/engineering development, We are more experienced in Scala, Java, SQL, Python. Hence the adoption of existing libraries like TerraGrunt (Go-based), InSpec (Ruby-based), etc. would burden the team in adopting these libraries. So keep it simple, I used Python.

### Implementation Logic
Before we proceed, I recommend giving a read on this article [Terraform - State demystified](https://thorsten-hans.com/terraform-state-demystified). It provides a good in-depth view of what is being maintained in the Terraform state file.

I prefer to keep things simple. Instead of suggesting to explore and learn a tool or library, I prefer to see what can be done using existing language libraries. Hence during this walkthrough, I choose only the following:
 - Python 3+
 - Pytest

With the Terraform State file being a JSON file, it is easy to achieve the outcome we want. We start by getting the state file locally. If you follow best practices, it is recommended that the state files be stored in a remote location like S3 or Azure Blob, etc. The [Terraform State Pull](https://www.terraform.io/docs/commands/state/pull.html) command can be used to download the state file.

Once downloaded, use the python JSON package to parse the file. Execute different test functions (ex based on pytest library) and assert if the deployed resources met the criteria.

### An example implementation
For the rest of the article, I will be walking through this verification process using code from <<my repo>>

#### Azure Resource
Below is the layout of resources that are deployed in Azure using Terraform. The scripts are found in the ‘HMAP-TFSTATE-RG’. 
![](./docs/images/deployment_layout.jpg)

Keeping it simple, we create a Storage Account, Key Vault, and an Azure Data Factory (ADF). The ADF is linked to the storage account and key Vault.

#### Verifications
As part of a sample we verify the following:
 - Is the storage account deployed?
 - Is the resource group deployed?
 - Is the ADF deployed?
 - Does ADF have a System Managed Identity?
 - Is the Storage account publicly accessible?

As reflected earlier, we parse the Terraform State file (TFStateParser.py). Once parsed we pass the instance to a specific test function that asserts for desired conditions.
The code for verifying if the storage account is publicly accessible is displayed below. Nothing fancy, just simple and plain. This code is present in ‘test/test_HMAP_TFSTATE_RG.py’.
![](./docs/images/sample_test_function.png)

The pytest framework takes care of executing the tests and creating a report, which we demonstrate in the below section.
### Example Run
I have the sample Terraform State file ‘examples/example_terraform.tfstate’; the private key pieces of information have been replaced with dummy values for obvious reasons.
The tests are executed using the following commands:
``sh
  pip install -r requirements.txt
  pytest -vr s --state_file=../examples/example_terraform.tfstate \ --html=../examples/test_report.html
``

The output of execution:
![](./docs/images/execution_run.jpg)

The report is also captured in an HTML file (examples/test_report.html):
![](./docs/images/pyttest_report_html.jpg)

As you can see this clearly reflects which of the configurations are conformant and which are not. This script does not also query Azure. Hence you could take the state any number of times and run these verifications to indicate any failures on future deployments.

In the above, one of the tests is Failed purposefully to demonstrate.

### Limitations
 - This verification is possible only for resources which has a Terraform provider. Resources like budget for which a provider is not available, this verification would not work.
 - As new features are added in Azure, it would take some time for those features to be added to the Terraform provider. Until such time these verifications cannot be done using this approach.

## Final Thoughts
This form of verification though simple could be thought of as an aid in your teams initial journey to IAC adoption.


