import TFStateParser
import pytest
from TFStateParser import TFStateParser

def pytest_addoption(parser):
    parser.addoption( "--state_file",
            action="append",
            default=[],
            help="The state file to inspect and verify."
            )

def pytest_generate_tests(metafunc):
    if "state_file" in metafunc.fixturenames:
        metafunc.parametrize("state_file", 
                metafunc.config.getoption("state_file"))


@pytest.fixture #(scope='session')
def tf_managed_rscs(state_file):
    tfp = TFStateParser()
    tfstate = tfp.parse(state_file)
    return tfp.get_managed_resources(tfstate)

