import sys, os, json, logging
import argparse

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)

class TFStateParser:

    def get_all_resources_of_type(self,p_rscs, p_type):
        return [ r for r in p_rscs if r['type'] == p_type]

    def get_instances_of_types(self, p_rscs,p_type):
        rscs = self.get_all_resources_of_type(p_rscs,p_type)
        rsc_defs = []
        for r in rscs:
            l = [inst['attributes'] for inst in r['instances']]
            rsc_defs.extend(l)
        return rsc_defs

    def parse(self, p_tfstate):
        logger.info('Parsing state file {} ...'.format(p_tfstate))
        with open(p_tfstate,'r') as fl:
          try:
            data = json.load(fl)
            return data
          except:
            e = sys.exc_info()[0]
            logger.error(e)
            raise e

    def get_managed_resources(self, p_state):
        rsc = p_state['resources']
        mrscs = [ r for r in rsc if r['mode'] == 'managed']
        return mrscs

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='TF State parser and verifier')
  parser.add_argument('-l', '--loglevel',nargs='?', help='log level, level is defaulted to INFO', default='INFO')
  parser.add_argument('-c', '--config', help='TF state file to parse')
  args = parser.parse_args()

  config_file = args.config		

  numeric_level = getattr(logging, args.loglevel.upper(), None)
  if not isinstance(numeric_level, int):
   raise ValueError('Invalid log level: %s' % loglevel)
  print('log level {}  => {}'.format(args.loglevel,numeric_level))
  logging.basicConfig() #level=numeric_level)
  logger.setLevel(numeric_level) #logging.DEBUG)

  tfp = TFStateParser()
  tfstate = tfp.parse(args.config)
  tfp.get_managed_resources(tfstate)

  logger.info('Finished !!!')
