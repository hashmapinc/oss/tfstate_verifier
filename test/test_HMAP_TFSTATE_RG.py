import pytest
import logging
from TFStateParser import TFStateParser

def test_are_storage_accounts_present(tf_managed_rscs):
    tf_parser = TFStateParser()
    sas = tf_parser.get_instances_of_types(tf_managed_rscs, 'azurerm_storage_account')
    sa_names = set([ r['name'] for r in sas])
    assert 'hmcacingeststa01' in sa_names

def test_rg_is_present(tf_managed_rscs):
    tf_parser = TFStateParser()
    rgs = tf_parser.get_instances_of_types(tf_managed_rscs, 'azurerm_resource_group')
    rg_names = set([ r['name'] for r in rgs])
    logging.info(rg_names)
    assert 'HMAP-TFSTATE-RG' in rg_names

def test_are_adf_present(tf_managed_rscs):
    tf_parser = TFStateParser()
    adfs = tf_parser.get_instances_of_types(tf_managed_rscs, 'azurerm_data_factory')
    assert len(adfs) == 1, "ADF were not deployed"

def test_does_the_adf_has_system_managed_identity(tf_managed_rscs):
    tf_parser = TFStateParser()
    rscs = tf_parser.get_instances_of_types(tf_managed_rscs, 'azurerm_data_factory')
    adf = [ r for r in rscs if r['name'] == 'hmcacingestadf01' ][0]
    logging.info(len(adf))
    assert adf['identity'][0]['type'] == 'SystemAssigned', "ADF instance does not have System Assigned Identity."

def test_storage_account_should_not_be_public_accessible(tf_managed_rscs):
    tf_parser = TFStateParser()
    sas = tf_parser.get_instances_of_types(tf_managed_rscs, 'azurerm_storage_account')
    sa = [ r for r in sas if r['name'] == 'hmcacingeststa01' ][0]

    assert sa['allow_blob_public_access'] == False
