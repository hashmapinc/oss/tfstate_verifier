
resource "azurerm_key_vault" "kv" {
  name = join("", [
    local.name_prefix,
    "kv01"
  ])

  tags = var.tags_base

  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  sku_name  = "standard"
  tenant_id = data.azurerm_client_config.current.tenant_id
}
