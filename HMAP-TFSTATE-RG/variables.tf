
variable "module_name" {
  default = "ingest"
}

variable "module_short_name" {
  default = "ing"
}

variable "azregion" {
  default = "Canada Central"
}

variable "azregion_short_code" {
  description = "region short code where resource is deployed"
  default     = "cac"
}

variable "tags_base" {
  default = {
    "DEVELOPER" = "venkat"
    "USE"       = "Blog"
  }
}
