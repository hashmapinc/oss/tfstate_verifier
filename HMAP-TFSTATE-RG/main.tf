
data "azurerm_client_config" "current" {}

locals {
  name_prefix = join("", ["hm",
    var.azregion_short_code,
  var.module_name])
}

resource "azurerm_resource_group" "rg" {
  name     = "HMAP-TFSTATE-RG"
  location = var.azregion
  tags     = var.tags_base
}
