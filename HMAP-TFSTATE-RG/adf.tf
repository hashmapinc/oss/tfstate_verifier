
resource "azurerm_data_factory" "adf_ingest" {
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  name = join("", [
    local.name_prefix,
    "adf01"
  ])

  tags = var.tags_base

  identity {
    type = "SystemAssigned"
  }
}

resource "azurerm_role_assignment" "adls_adf_role" {
  depends_on = [ azurerm_storage_account.adls2 ]
  scope = azurerm_storage_account.adls2.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id = azurerm_data_factory.adf_ingest.identity[0].principal_id
}

resource "azurerm_data_factory_linked_service_data_lake_storage_gen2" "adlsg2_ls" {
  depends_on = [ azurerm_role_assignment.adls_adf_role ]

  name = join("", [
    azurerm_storage_account.adls2.name,
  "_ls"])

  resource_group_name = azurerm_resource_group.rg.name
  data_factory_name   = azurerm_data_factory.adf_ingest.name

  url                  = azurerm_storage_account.adls2.primary_dfs_endpoint
  use_managed_identity = false
}
