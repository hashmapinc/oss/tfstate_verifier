
resource "azurerm_storage_account" "adls2" {
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  name = join("", [
    local.name_prefix,
    "sta01"
  ])

  tags                     = var.tags_base
  account_tier             = "Standard"
  account_replication_type = "LRS"

  identity {
    type = "SystemAssigned"
  }

  #allow_blob_public_access = true
}
